use data::{
    ConnectHeader, DisconnectHeader, FixedHeader, Packet, Payload, Properties, PublishHeader,
    SubscribeHeader, UnsubscribeHeader, VarHeader,
};
use std::ops::Deref;
use std::vec::IntoIter;

pub struct Client {
    requests: IntoIter<data::Packet>,
}

impl Client {
    pub fn new() -> Client {
        Client {
            requests: get_messages().into_iter(),
        }
    }

    pub fn next_message(&mut self) -> Option<Packet> {
        match self.requests.next() {
            Some(m) => Some(m),
            None => None,
        }
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}

#[allow(dead_code)]
pub fn get_hash_messages() -> Vec<Packet> {
    let mut ret = vec![];
    ret.push(connect_packet());
    ret.push(subscribe_packet(1, "#"));
    ret.push(subscribe_packet(2, "topic-1"));
    ret.push(subscribe_packet(3, "topic-1/#"));
    ret.push(subscribe_packet(4, "topic-1/foo"));
    ret.push(publish_packet(1, "topic-1", "test1"));
    ret.push(publish_packet(1, "topic-1/foo", "testFoo"));
    ret.push(publish_packet(1, "topic-1/bar", "testBar"));
    ret.push(publish_packet(1, "topic-1/bar/foo", "testBarFoo"));
    ret.push(publish_packet(1, "topic-1/foo/bar", "testFooBar"));
    ret.push(publish_packet(1, "topic-2/foo/bar", "testFooBar"));
    ret.push(disconnect_packet());
    ret
}

#[allow(dead_code)]
pub fn get_messages() -> Vec<Packet> {
    let mut ret = vec![];
    ret.push(connect_packet());
    ret.push(publish_packet(1, "topic-1", "test0"));
    ret.push(subscribe_packet(2, "topic-1"));
    ret.push(subscribe_packet(3, "topic-2"));
    ret.push(publish_packet(1, "topic-1", "test1"));
    ret.push(publish_packet(1, "topic-2", "test3"));
    ret.push(subscribe_packet(6, "topic-1"));
    ret.push(subscribe_packet(7, "topic-1/#"));
    ret.push(subscribe_packet(8, "topic-1/foo/+"));
    ret.push(subscribe_packet(9, "topic-1/+/foo"));
    ret.push(publish_packet(1, "topic-1", "test1"));
    ret.push(publish_packet(1, "topic-1/foo", "testFoo"));
    ret.push(publish_packet(1, "topic-1/bar", "testBar"));
    ret.push(publish_packet(1, "topic-1/foo/1", "testFoo1"));
    ret.push(publish_packet(1, "topic-1/foo/2", "testFoo2"));
    ret.push(publish_packet(1, "topic-1/1/foo", "test1Foo"));
    ret.push(publish_packet(1, "topic1/2/foo", "test2Foo"));
    ret.push(publish_packet(1, "topic-1", "test1"));
    ret.push(publish_packet(1, "topic-1/foo", "testFoo"));
    ret.push(publish_packet(1, "topic-1/foo", "testFoo"));
    ret.push(publish_packet(1, "topic-1/bar/foo", "testBarFoo"));
    ret.push(publish_packet(1, "topic-1/foo/bar", "testFooBar"));
    ret.push(publish_packet(1, "topic-2/foo/bar", "testFooBar"));
    ret.push(unsubscribe_packet(2, "topic-1"));
    ret.push(unsubscribe_packet(2, "topic-2"));
    ret.push(publish_packet(1, "topic-1", "test1"));
    ret.push(unsubscribe_packet(3, "topic-2"));
    ret.push(unsubscribe_packet(6, "topic-1"));
    ret.push(publish_packet(1, "topic-1", "test2"));
    ret.push(publish_packet(1, "topic-2", "test2"));
    ret.push(disconnect_packet());
    ret
}

fn subscribe_packet(id: u16, topic: &str) -> Packet {
    let mut payload = vec![0, topic.len() as u8];
    payload.extend(topic.as_bytes());
    payload.push(1);
    let var = Box::new(SubscribeHeader::new(
        id,
        Properties::new(2, vec![0x0B, id as u8]),
    ));
    let p_s = payload.len();
    let p = Payload::new(payload);
    let rem = (var.deref() as &dyn VarHeader).data().len() + p_s;
    Packet::new(FixedHeader::new(8, 0, rem), var, p)
}

fn publish_packet(id: u16, topic: &str, payload: &str) -> Packet {
    let var = Box::new(PublishHeader::new(
        String::from(topic),
        id,
        Properties::new(0, vec![]),
    ));
    let p = Payload::new(payload.as_bytes().to_vec());
    let rem = (var.deref() as &dyn VarHeader).data().len() + payload.len();
    Packet::new(FixedHeader::new(3, 0, rem), var, p)
}

fn unsubscribe_packet(id: u16, topic: &str) -> Packet {
    let mut payload = vec![0, topic.len() as u8];
    payload.extend(topic.as_bytes());
    payload.push(1);
    let var = Box::new(UnsubscribeHeader::new(id, Properties::new(0, vec![])));
    let p_s = payload.len();
    let p = Payload::new(payload);
    let rem = (var.deref() as &dyn VarHeader).data().len() + p_s;
    Packet::new(FixedHeader::new(10, 0, rem), var, p)
}

fn connect_packet() -> Packet {
    Packet::new(
        FixedHeader::new(1, 0, 2),
        Box::new(ConnectHeader::new(Properties::new(0, vec![]))),
        Payload::new(vec![]),
    )
}

fn disconnect_packet() -> Packet {
    Packet::new(
        FixedHeader::new(14, 0, 2),
        Box::new(DisconnectHeader::new(Properties::new(0, vec![]))),
        Payload::new(vec![]),
    )
}
