extern crate pretty_env_logger;
use log::{debug, info, LevelFilter};

use client::Client;

use data::Packet;
use futures_util::sink::SinkExt;
use futures_util::stream::StreamExt;
use tokio::net::TcpStream;
use tokio_util::codec::Decoder;

fn main() {
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Info)
        .init();

    let mut rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(async { async_main().await })
}

async fn async_main() {
    // Start up the clients, each in its own thread
    let one = tokio::spawn(async { run_client().await });
    let two = tokio::spawn(async { run_client().await });
    let three = tokio::spawn(async { run_client().await });
    let four = tokio::spawn(async { run_client().await });
    let five = tokio::spawn(async { run_client().await });

    tokio::try_join!(one, two, three, four, five).unwrap();
}

async fn run_client() {
    let mut client: Client = Client::new();
    let stream = TcpStream::connect("127.0.0.1:27182").await.unwrap();

    let codec = Packet::new_empty();
    let (mut sink, mut stream) = codec.framed(stream).split();

    loop {
        let m = client.next_message();
        if m.is_none() {
            info!("No more packets to send");
            break;
        }

        let m = m.unwrap();
        info!("Sending {:?}", m);
        sink.send(m).await.unwrap();
        let resp = stream.next().await.unwrap().unwrap();
        info!("Got response: {:?}", resp);
    }
}
