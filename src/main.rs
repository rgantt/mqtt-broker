mod generator;
mod server;

extern crate pretty_env_logger;
use log::{info, LevelFilter};

use server::Server;
use std::error::Error;

use std::net::SocketAddr;

use data::{
    AuthHeader, ConnAckHeader, ConnectHeader, DisconnectHeader, FixedHeader, Message, Packet,
    Payload, PingRespHeader, Properties, PubAckHeader, PublishHeader, SubAckHeader, UnsubAckHeader,
    VarHeader,
};
use futures_util::{SinkExt, StreamExt};
use tokio::net::{TcpListener, TcpStream};
use tokio::sync::mpsc::{unbounded_channel, UnboundedSender};
use tokio_util::codec::Decoder;
use tokio_util::codec::Framed;

pub struct Connection {
    pub buf: Vec<u8>,
    pub addr: SocketAddr,
}

fn main() -> Result<(), Box<dyn Error>> {
    pretty_env_logger::formatted_builder()
        .filter_level(LevelFilter::Info)
        .init();

    let mut rt = tokio::runtime::Runtime::new().unwrap();
    rt.block_on(async { async_main().await })
}

async fn async_main() -> Result<(), Box<dyn Error>> {
    let (server_tx, mut server_rx) =
        unbounded_channel::<(SocketAddr, Message, UnboundedSender<Message>)>();

    tokio::spawn(async move {
        let mut server = Server::new();

        while let Some((addr, message, sender)) = server_rx.recv().await {
            info!("Got message on server receive: {:?}", message);

            server.accept(addr, sender, message);
        }
    });

    let mut listener = TcpListener::bind("0.0.0.0:27182").await.unwrap();
    loop {
        let (socket, _) = listener.accept().await.unwrap();
        info!("Got connection from {:?}", socket);

        // Need to clone this before it's moved
        let server_tx = server_tx.clone();
        tokio::spawn(async { handle_conn(socket, server_tx).await });
    }
}

async fn handle_conn(
    stream: TcpStream,
    sender: UnboundedSender<(SocketAddr, Message, UnboundedSender<Message>)>,
) {
    let peer = stream.peer_addr().unwrap();
    let (mut sink, mut stream) = Packet::new_empty().framed(stream).split();

    let (tx, mut rx) = unbounded_channel();

    tokio::spawn(async move {
        while let Some(message) = rx.recv().await {
            info!("Got message on channel receive: {:?}", message);
            let packet: Packet = packet_from_message(message);
            sink.send(packet).await.unwrap();
            info!("Sent message to sink");
        }
    });

    while let Some(Ok(packet)) = stream.next().await {
        let tx = tx.clone();
        let message = message_from_packet(packet);
        sender.send((peer, message, tx)).unwrap();
    }

    info!("Connected from {:?} terminated", peer);
}

fn packet_from_message(message: Message) -> Packet {
    let var: Box<dyn VarHeader> = match &message {
        Message::CONNACK => Box::new(ConnAckHeader::new(Properties::new(0, vec![]))),
        Message::PUBLISH {
            topic: t,
            identifier: i,
            ..
        } => Box::new(PublishHeader::new(
            t.clone(),
            *i,
            Properties::new(0, vec![]),
        )),
        Message::PUBACK { identifier } => {
            Box::new(PubAckHeader::new(*identifier, Properties::new(0, vec![])))
        }
        Message::SUBACK => Box::new(SubAckHeader::new(Properties::new(0, vec![]))),
        Message::UNSUBACK => Box::new(UnsubAckHeader::new(Properties::new(0, vec![]))),
        Message::PINGRESP => Box::new(PingRespHeader::new()),
        Message::DISCONNECT => Box::new(DisconnectHeader::new(Properties::new(0, vec![]))),
        Message::AUTH => Box::new(AuthHeader::new(Properties::new(0, vec![]))),
        _ => panic!("Unrecognized packet type: {:?}", message),
    };

    let packet_type = match &message {
        Message::CONNACK => 2,
        Message::PUBLISH { .. } => 3,
        Message::PUBACK { .. } => 4,
        Message::SUBACK => 9,
        Message::UNSUBACK => 11,
        Message::PINGRESP => 13,
        Message::DISCONNECT => 14,
        Message::AUTH => 15,
        _ => panic!("Unrecognized packet type: {:?}", message),
    };

    // LOL FIXME
    let mut remaining_len = match &message {
        Message::PINGRESP => 0,
        Message::PUBLISH { .. } => var.data().len(),
        _ => 2,
    };

    // Need this to match the incoming QoS from the client--or, alternatively, remove the PKID
    // information when we encode the variable header
    let lsb = match &message {
        Message::PUBLISH { .. } => 2,
        _ => 0,
    };

    let payload = match message {
        Message::PUBLISH { message: m, .. } => Payload::new(m),
        _ => Payload::new(vec![]),
    };

    remaining_len += &payload.as_ref().unwrap().data.len();

    let packet = Packet::new(
        FixedHeader::new(packet_type, lsb, remaining_len),
        var,
        payload,
    );
    info!("Created packet {:?}", packet);
    packet
}

fn message_from_packet(packet: Packet) -> Message {
    let packet_type = &packet.fixed.as_ref().unwrap().packet_type;
    match packet_type {
        1 => Message::CONNECT,
        3 => {
            // PUBLISH
            let data = packet.payload.unwrap().data;
            let variable = packet.variable.unwrap();
            Message::PUBLISH {
                topic: String::from(variable.topic()),
                message: data,
                identifier: *variable.identifier(),
            }
        }
        4 => {
            // PUBACK
            let variable = packet.variable.unwrap();
            Message::PUBACK {
                identifier: *variable.identifier(),
            }
        }
        8 => {
            // SUBSCRIBE
            let data = packet.payload.unwrap().data;
            // let remaining_len = &packet.fixed.unwrap().remaining_len;
            let variable = packet.variable.unwrap();
            Message::SUBSCRIBE {
                identifier: *variable.identifier(),
                topic: String::from(std::str::from_utf8(&data[1..data.len() - 1]).unwrap()),
            }
        }
        10 => {
            // UNSUBSCRIBE
            let data = packet.payload.unwrap().data;
            let offset = 2 + (data[0] + data[1]) as usize;
            Message::UNSUBSCRIBE {
                identifier: *packet.variable.unwrap().identifier(),
                topic: String::from(std::str::from_utf8(&data[2..offset]).unwrap()),
            }
        }
        12 => Message::PINGREQ,
        14 => Message::DISCONNECT,
        s => panic!("Unrecognized packet type: {:?}", s),
    }
}
