use data::Message;
use log::info;
use std::net::SocketAddr;
use tokio::sync::mpsc::UnboundedSender;
use topic_tree::TopicFilterTree;

use log::error;
use std::collections::HashMap;

pub struct Server {
    pub(crate) subs: TopicFilterTree<Subscription>,
    conns: HashMap<SocketAddr, Context>,
}

impl Server {
    pub fn new() -> Self {
        Server {
            subs: TopicFilterTree::new(),
            conns: HashMap::new(),
        }
    }

    pub fn accept(&mut self, addr: SocketAddr, sender: UnboundedSender<Message>, m: Message) {
        let conn = self.conns.entry(addr).or_insert(Context::new());

        conn.accept(sender, m, &mut self.subs);
    }
}

#[derive(Clone, Debug)]
pub struct Subscription {
    topic: String,
    identifier: u16,
    sender: UnboundedSender<Message>,
}

impl PartialEq for Subscription {
    fn eq(&self, other: &Self) -> bool {
        self.identifier == other.identifier
    }
}

impl Subscription {
    pub fn deliver(&mut self, t: String, m: &[u8], i: u16) {
        let msg = Message::PUBLISH {
            topic: t,
            message: m.to_vec(),
            identifier: i,
        };
        info!("Delivering to {:?} to {}", msg, self.identifier);
        match self.sender.send(msg) {
            Err(e) => error!("Could not deliver publish! {:?}", e),
            _ => {}
        }
    }
}

pub struct Context {
    pub state: Box<dyn Connection>,
}

// FIXME: Shouldn't need this
unsafe impl Send for Context {}

#[derive(Copy, Clone)]
pub struct Acceptor<T> {
    value: Option<T>,
}

impl<T> Acceptor<T> {
    pub fn new() -> Self {
        Acceptor { value: None }
    }

    pub fn accept(&mut self, value: T) {
        self.value = Some(value);
    }

    pub fn get(&mut self) -> Option<T> {
        self.value.take()
    }
}

impl Context {
    pub fn new() -> Self {
        Context {
            state: Box::from(UninitializedConnection {}),
        }
    }

    pub fn accept(
        &mut self,
        sender: UnboundedSender<Message>,
        m: Message,
        subs: &mut TopicFilterTree<Subscription>,
    ) {
        let mut acceptor = Acceptor::new();
        self.state.accept(sender.clone(), &mut acceptor, subs, m);

        if let Some(state) = acceptor.get() {
            self.state = state;
        }
    }
}

#[derive(Clone, Copy)]
struct UninitializedConnection;
#[derive(Clone, Copy)]
struct InitializedConnection;

impl UninitializedConnection {
    pub fn connect(
        self,
        state: &mut Acceptor<Box<dyn Connection>>,
        sender: UnboundedSender<Message>,
    ) {
        info!("Established connection!");

        state.accept(Box::from(InitializedConnection {}));
        match sender.send(Message::CONNACK) {
            Err(e) => error!("Could not send ack: {:?}", e),
            _ => {}
        }
    }
}

impl InitializedConnection {
    pub fn subscribe(
        &self,
        sender: UnboundedSender<Message>,
        subs: &mut TopicFilterTree<Subscription>,
        t: &str,
        i: u16,
    ) {
        info!("Registering subscription {} to {}", i, t);
        subs.insert(
            &t,
            Subscription {
                identifier: i,
                topic: t.to_string(),
                sender: sender.clone(),
            },
        )
        .expect("Could not register subscription!");
        match sender.send(Message::SUBACK) {
            Err(e) => error!("Could not send ack: {:?}", e),
            _ => {}
        }
    }

    pub fn unsubscribe(
        &self,
        sender: UnboundedSender<Message>,
        subs: &mut TopicFilterTree<Subscription>,
        t: &str,
        i: u16,
    ) {
        info!("Unsubscribing {} from {}", i, t);
        subs.remove(
            t,
            Subscription {
                identifier: i,
                topic: t.to_string(),
                sender: sender.clone(),
            },
        );
        match sender.send(Message::UNSUBACK) {
            Err(e) => error!("Could not send ack: {:?}", e),
            _ => {}
        }
    }

    pub fn publish(
        &self,
        sender: UnboundedSender<Message>,
        subs: &mut TopicFilterTree<Subscription>,
        t: &str,
        m: &[u8],
        identifier: u16,
    ) {
        info!("Received message {} for {}", identifier, t);
        for mut sub in subs.find(&t) {
            sub.deliver(String::from(t), m, identifier)
        }
        match sender.send(Message::PUBACK { identifier }) {
            Err(e) => error!("Could not send ack: {:?}", e),
            _ => {}
        }
    }

    pub fn disconnect(self, acceptor: &mut Acceptor<Box<dyn Connection>>) {
        info!("Disconnected!");
        acceptor.accept(Box::from(UninitializedConnection {}));
    }

    pub fn pong(self, sender: UnboundedSender<Message>) {
        match sender.send(Message::PINGRESP) {
            Err(e) => error!("Could not send pong: {:?}", e),
            _ => {}
        }
    }
}

pub trait Connection {
    fn accept(
        &self,
        sender: UnboundedSender<Message>,
        state: &mut Acceptor<Box<dyn Connection>>,
        subs: &mut TopicFilterTree<Subscription>,
        m: Message,
    );
}

impl Connection for UninitializedConnection {
    fn accept(
        &self,
        sender: UnboundedSender<Message>,
        state: &mut Acceptor<Box<dyn Connection>>,
        _subs: &mut TopicFilterTree<Subscription>,
        m: Message,
    ) {
        match m {
            Message::CONNECT => self.connect(state, sender),
            _ => panic!("Unexpected message: {:?}", m),
        }
    }
}

impl Connection for InitializedConnection {
    fn accept(
        &self,
        sender: UnboundedSender<Message>,
        state: &mut Acceptor<Box<dyn Connection>>,
        subs: &mut TopicFilterTree<Subscription>,
        m: Message,
    ) {
        match m {
            Message::PUBLISH {
                topic: t,
                message: m,
                identifier: i,
            } => self.publish(sender, subs, &t, &m, i),
            Message::SUBSCRIBE {
                topic: t,
                identifier: i,
            } => self.subscribe(sender, subs, &t, i),
            Message::UNSUBSCRIBE {
                topic: t,
                identifier: i,
            } => self.unsubscribe(sender, subs, &t, i),
            Message::DISCONNECT => self.disconnect(state),
            Message::PINGREQ => self.pong(sender),
            Message::PUBACK { identifier } => info!("PUBACK for {:?}", identifier),
            _ => panic!("Unexpected message: {:?}", m),
        }
    }
}
