use log::{debug, info};

use bytes::BytesMut;
use std::fmt::Formatter;
use tokio_util::codec::{Decoder, Encoder};

#[derive(Debug)]
pub enum Message {
    CONNECT,
    CONNACK,
    PUBLISH {
        topic: String,
        message: Vec<u8>,
        identifier: u16,
    },
    PUBACK {
        identifier: u16,
    },
    SUBSCRIBE {
        topic: String,
        identifier: u16,
    },
    SUBACK,
    UNSUBSCRIBE {
        topic: String,
        identifier: u16,
    },
    UNSUBACK,
    PINGREQ,
    PINGRESP,
    DISCONNECT,
    AUTH,
}

pub struct Payload {
    pub data: Vec<u8>,
}

impl Payload {
    pub fn new(data: Vec<u8>) -> Option<Self> {
        Some(Payload { data })
    }
    pub fn size(&self) -> usize {
        self.data.len()
    }
}

pub struct Packet {
    pub buf: Vec<u8>,
    pub fixed: Option<FixedHeader>,
    pub variable: Option<Box<dyn VarHeader>>,
    pub payload: Option<Payload>,
}

impl std::fmt::Debug for Packet {
    fn fmt(&self, fmt: &mut Formatter<'_>) -> Result<(), std::fmt::Error> {
        write!(fmt, "{0}", &self.fixed.as_ref().unwrap().packet_type)
    }
}

impl Packet {
    pub fn new(fixed: FixedHeader, variable: Box<dyn VarHeader>, payload: Option<Payload>) -> Self {
        Packet {
            fixed: Some(fixed),
            variable: Some(variable),
            payload,
            buf: vec![],
        }
    }

    pub fn new_empty() -> Self {
        Packet {
            fixed: None,
            variable: None,
            payload: None,
            buf: vec![],
        }
    }
}

impl Encoder<Packet> for Packet {
    type Error = std::io::Error;

    fn encode(&mut self, event: Packet, buf: &mut BytesMut) -> Result<(), Self::Error> {
        let tmp = &event.fixed.unwrap().bytes();
        buf.extend_from_slice(tmp);
        buf.extend_from_slice(&event.variable.unwrap().data());
        if event.payload.is_some() {
            buf.extend_from_slice(&event.payload.as_ref().unwrap().data);
        }
        info!("{:02X?}", &buf.to_vec());
        Ok(())
    }
}

impl Decoder for Packet {
    type Item = Packet;
    type Error = std::io::Error;

    fn decode(&mut self, src: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        info!("{:02X?}", &src.to_vec());
        // Need two bytes to extract a fixed header
        if src.len() < 2 {
            debug!("Need at least 2 bytes");
            return Ok(None);
        }
        let fixed = FixedHeader::from_bytes(&src.split_to(2).freeze());

        let required = fixed.remaining_len;
        if required > src.len() {
            debug!("Header needs {} bytes, but has {}", required, src.len());
            return Ok(None);
        }

        let buf = src.split_to(required).freeze();
        let var: Box<dyn VarHeader> = match fixed.packet_type {
            1 => Box::new(ConnectHeader::from_bytes(&buf[..])),
            2 => Box::new(ConnAckHeader::from_bytes(&buf[..])),
            3 => Box::new(PublishHeader::from_bytes(&buf[..])),
            4 => Box::new(PubAckHeader::from_bytes(&buf[..])),
            8 => Box::new(SubscribeHeader::from_bytes(&buf[..])),
            9 => Box::new(SubAckHeader::from_bytes(&buf[..])),
            10 => Box::new(UnsubscribeHeader::from_bytes(&buf[..])),
            11 => Box::new(UnsubAckHeader::from_bytes(&buf[..])),
            12 => Box::new(PingReqHeader::from_bytes(&buf[..])),
            13 => Box::new(PingRespHeader::from_bytes(&buf[..])),
            14 => Box::new(DisconnectHeader::from_bytes(&buf[..])),
            15 => Box::new(AuthHeader::from_bytes(&buf[..])),
            _ => panic!("Unsupported packet type {}!", fixed.packet_type),
        };

        let data = if buf.len() > 0 {
            buf[var.data().len()..].to_vec()
        } else {
            vec![]
        };
        Ok(Some(Packet {
            fixed: Some(fixed),
            payload: Payload::new(data),
            variable: Some(var),
            buf: vec![],
        }))
    }
}

unsafe impl Send for Packet {}

#[derive(Debug)]
pub struct FixedHeader {
    pub packet_type: u8,
    pub packet_flags: u8,
    pub remaining_len: usize,
}

impl FixedHeader {
    pub fn new(packet_type: u8, packet_flags: u8, remaining_len: usize) -> Self {
        FixedHeader {
            packet_type,
            packet_flags,
            remaining_len,
        }
    }

    #[allow(clippy::identity_op)]
    pub fn from_bytes(bytes: &[u8]) -> Self {
        Self::new(
            (bytes[0] >> 4) & 0x0f,
            (bytes[0] >> 0) & 0x0f,
            bytes[1] as usize,
        )
    }

    #[allow(clippy::identity_op)]
    pub fn bytes(&mut self) -> Vec<u8> {
        vec![
            ((self.packet_type << 4) | (self.packet_flags << 0)) as u8,
            self.remaining_len as u8,
        ]
    }
}

pub struct Properties {
    pub len: usize,
    pub properties: Vec<u8>,
}

impl Properties {
    pub fn new(len: usize, properties: Vec<u8>) -> Self {
        Properties { len, properties }
    }
}

pub struct PublishHeader {
    pub topic: String,
    pub identifier: u16,
    pub properties: Properties,
}

impl VarHeader for PublishHeader {
    fn identifier(&self) -> &u16 {
        &self.identifier
    }

    fn reason(&self) -> &u8 {
        unimplemented!()
    }

    fn topic(&self) -> &str {
        &self.topic
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    #[allow(clippy::identity_op)]
    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        let len = self.topic.len();
        res.push((len >> 8) as u8);
        res.push((len >> 0) as u8);
        res.extend(self.topic.as_bytes());
        res.push((self.identifier >> 8) as u8);
        res.push((self.identifier >> 0) as u8);
        // res.push(self.properties.len as u8);
        // res.extend(self.properties.properties.clone());
        res
    }
}

impl PublishHeader {
    pub fn new(topic: String, identifier: u16, properties: Properties) -> Self {
        PublishHeader {
            topic,
            identifier,
            properties,
        }
    }

    pub fn from_bytes(input: &[u8]) -> Self {
        let topic_len = (input[0] + input[1]) as usize;
        let identifier = (input[2 + topic_len] + input[2 + topic_len + 1]) as u16;
        PublishHeader {
            topic: String::from(std::str::from_utf8(&input[2..2 + topic_len]).unwrap()),
            identifier,
            properties: Properties::new(0, vec![]),
        }
    }
}

pub trait VarHeader {
    fn identifier(&self) -> &u16;
    fn reason(&self) -> &u8;
    fn topic(&self) -> &str;
    fn property_len(&self) -> &u8;
    fn properties(&self) -> &Properties;
    fn data(&self) -> Vec<u8>;
}

pub struct SubscribeHeader {
    pub identifier: u16,
    pub properties: Properties,
}

impl VarHeader for SubscribeHeader {
    fn identifier(&self) -> &u16 {
        &self.identifier
    }

    fn reason(&self) -> &u8 {
        unimplemented!()
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    #[allow(clippy::identity_op)]
    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push((self.identifier >> 8) as u8);
        res.push((self.identifier >> 0) as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl SubscribeHeader {
    pub fn new(identifier: u16, properties: Properties) -> Self {
        SubscribeHeader {
            identifier,
            properties,
        }
    }

    pub fn from_bytes(data: &[u8]) -> Self {
        let identifier = (data[0] + data[1]) as u16;
        SubscribeHeader {
            identifier,
            properties: Properties::new(0, vec![]),
        }
    }
}

pub struct UnsubscribeHeader {
    pub identifier: u16,
    pub properties: Properties,
}

impl VarHeader for UnsubscribeHeader {
    fn identifier(&self) -> &u16 {
        &self.identifier
    }

    fn reason(&self) -> &u8 {
        unimplemented!()
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    #[allow(clippy::identity_op)]
    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push((self.identifier >> 8) as u8);
        res.push((self.identifier >> 0) as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl UnsubscribeHeader {
    pub fn new(identifier: u16, properties: Properties) -> Self {
        UnsubscribeHeader {
            identifier,
            properties,
        }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        UnsubscribeHeader {
            identifier: 0,
            properties: Properties::new(0, vec![]),
        }
    }
}

pub struct ConnAckHeader {
    pub properties: Properties,
}

impl VarHeader for ConnAckHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push(*self.reason() as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl ConnAckHeader {
    pub fn new(properties: Properties) -> Self {
        ConnAckHeader { properties }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new(Properties::new(0, vec![]))
    }
}

pub struct PubAckHeader {
    pub identifier: u16,
    pub properties: Properties,
}

impl VarHeader for PubAckHeader {
    fn identifier(&self) -> &u16 {
        &self.identifier
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push((self.identifier >> 8) as u8);
        res.push((self.identifier >> 0) as u8);

        // These can be omitted if reason code is success
        let reason = *self.reason();
        if reason != 0x00 {
            res.push(*self.reason() as u8);
        }
        let len = self.properties.len;
        if len > 0 {
            res.push(self.properties.len as u8);
            res.extend(self.properties.properties.clone());
        }
        res
    }
}

impl PubAckHeader {
    pub fn new(identifier: u16, properties: Properties) -> Self {
        PubAckHeader {
            identifier,
            properties,
        }
    }

    pub fn from_bytes(data: &[u8]) -> Self {
        let identifier = (data[0] + data[1]) as u16;
        Self::new(identifier, Properties::new(0, vec![]))
    }
}

pub struct AuthHeader {
    pub properties: Properties,
}

impl VarHeader for AuthHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push(*self.reason() as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl AuthHeader {
    pub fn new(properties: Properties) -> Self {
        AuthHeader { properties }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new(Properties::new(0, vec![]))
    }
}

pub struct PingRespHeader {}

impl VarHeader for PingRespHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        unimplemented!()
    }

    fn data(&self) -> Vec<u8> {
        vec![]
    }
}

impl PingRespHeader {
    pub fn new() -> Self {
        PingRespHeader {}
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new()
    }
}

pub struct PingReqHeader {}

impl VarHeader for PingReqHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        unimplemented!()
    }

    fn data(&self) -> Vec<u8> {
        vec![]
    }
}

impl PingReqHeader {
    pub fn new() -> Self {
        PingReqHeader {}
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new()
    }
}

pub struct SubAckHeader {
    pub properties: Properties,
}

impl VarHeader for SubAckHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push(*self.reason() as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl SubAckHeader {
    pub fn new(properties: Properties) -> Self {
        SubAckHeader { properties }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new(Properties::new(0, vec![]))
    }
}

pub struct UnsubAckHeader {
    pub properties: Properties,
}

impl VarHeader for UnsubAckHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push(*self.reason() as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl UnsubAckHeader {
    pub fn new(properties: Properties) -> Self {
        UnsubAckHeader { properties }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new(Properties::new(0, vec![]))
    }
}

pub struct ConnectHeader {
    pub properties: Properties,
}

impl VarHeader for ConnectHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push(*self.reason() as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl ConnectHeader {
    pub fn new(properties: Properties) -> Self {
        ConnectHeader { properties }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new(Properties::new(0, vec![]))
    }
}

pub struct DisconnectHeader {
    pub properties: Properties,
}

impl VarHeader for DisconnectHeader {
    fn identifier(&self) -> &u16 {
        unimplemented!()
    }

    fn reason(&self) -> &u8 {
        &0x00
    }

    fn topic(&self) -> &str {
        unimplemented!()
    }

    fn property_len(&self) -> &u8 {
        unimplemented!()
    }

    fn properties(&self) -> &Properties {
        &self.properties
    }

    fn data(&self) -> Vec<u8> {
        let mut res = vec![];
        res.push(*self.reason() as u8);
        res.push(self.properties.len as u8);
        res.extend(self.properties.properties.clone());
        res
    }
}

impl DisconnectHeader {
    pub fn new(properties: Properties) -> Self {
        DisconnectHeader { properties }
    }

    pub fn from_bytes(_data: &[u8]) -> Self {
        Self::new(Properties::new(0, vec![]))
    }
}
