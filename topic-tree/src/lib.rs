use std::error::Error;
use std::fmt::Debug;

// An n-ary tree data structure that holds topic (wildcard) information for subscribers.
//
// In an MQTT topic, levels are delimited by slashes; insertion into this tree follows that
// convention. If you want to re-use the structure for something other than strings, I don't
// think the abstraction would be too tricky. Hopefully I'll do it when I learn Rust better.
//
// Interior nodes contain data identifying their level: the find algorithm is sensitive to these
// for the purpose of satisfying the MQTT 5.0 spec. Each node--interior and leaf--also holds a
// vector of subscriber references.
//
// Note: the current implementation does O(n) searches at each level; easy improvement to
// store children in lexicographical order and use binary search (then we're just talking
// fancy B+tree, rather than hand-rolled monstrosity).
pub struct TopicFilterTree<T> {
    root: Node<T>,
}

// TopicFilterTree Node
struct Node<T> {
    children: Vec<Node<T>>,
    data: String,
    subscribers: Vec<T>,
}

// Default impl since the structure doesn't require any parameters
impl<T: Debug + Clone + PartialEq> Default for TopicFilterTree<T> {
    fn default() -> Self {
        Self::new()
    }
}

impl<T: Debug + Clone + PartialEq> TopicFilterTree<T> {
    pub fn new() -> Self {
        TopicFilterTree {
            root: Node {
                children: Vec::new(),
                data: String::from("/"),
                subscribers: Vec::new(),
            },
        }
    }

    pub fn insert(&mut self, filter: &str, t: T) -> Result<(), Box<dyn Error>> {
        if filter.is_empty() {
            return Err("Topic name must be at least one character long".into());
        }
        if filter.find(std::char::from_u32(0x0000).unwrap()).is_some() {
            return Err("Topic names cannot contain the null character".into());
        }

        // Check for malformed topic wildcards
        if let Some(index) = filter.find('#') {
            if index != filter.len() - 1 {
                return Err("# wildcard must be the last character in the topic filter".into());
            }
            if filter.find("/#").is_none() && filter.len() > 2 {
                return Err("# wildcard must be used to replace an entire level".into());
            }
        }

        let mut node = &mut self.root;
        for part in filter.split('/') {
            let index = match node.children.iter().position(|n| n.data.eq(part)) {
                Some(i) => i,
                None => {
                    node.children.push(Node {
                        children: Vec::new(),
                        data: String::from(part),
                        subscribers: Vec::new(),
                    });
                    node.children.len() - 1
                }
            };
            node = &mut { node }.children[index];
        }
        node.subscribers.push(t);
        Ok(())
    }

    pub fn remove(&mut self, filter: &str, t: T) {
        let mut node = &mut self.root;
        for part in filter.split('/') {
            let index = match node.children.iter().position(|n| n.data.eq(part)) {
                Some(i) => i,
                None => {
                    node.children.push(Node {
                        children: Vec::new(),
                        data: String::from(part),
                        subscribers: Vec::new(),
                    });
                    node.children.len() - 1
                }
            };
            node = &mut { node }.children[index];
        }
        node.subscribers.retain(|s| !s.eq(&t))
    }

    pub fn find(&mut self, filter: &str) -> Vec<T> {
        // borrow checker hax
        let tmp = &self.root;
        let mut tot = vec![];
        // Clippy wants me to change this to &[&str] but I don't see how it could possibly work
        let res: &Vec<&str> = &filter.split('/').collect();
        Self::find_aux(0, &res, tmp, &mut tot);
        tot
    }

    fn find_aux(depth: u8, parts: &Vec<&str>, node: &Node<T>, total: &mut Vec<T>) {
        // Recursive base case; leaf node
        if parts.is_empty() {
            total.extend(node.subscribers.iter().cloned());

            // "sport/#" also matches "sport", since # includes the parent level
            if let Some(i) = node.children.iter().position(|n| "#".eq(&n.data)) {
                total.extend(node.children[i].subscribers.iter().cloned());
            }
            return;
        }

        let part = &parts.first().unwrap().to_string();
        // $ topics: https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901246
        let should_match = !((depth == 0) && part.starts_with('$'));

        // wildcards: https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901244

        // Search for wildcard matches; let's do # first since there's no need to recurse
        if let Some(i) = node.children.iter().position(|n| "#".eq(&n.data)) {
            if should_match {
                total.extend(node.children[i].subscribers.iter().cloned());
            }
        }

        let rest: Vec<&str> = Vec::from(&parts[1..]);

        // Now let's try +, which has us recurse without actually matching the current level
        if let Some(i) = node.children.iter().position(|n| "+".eq(&n.data)) {
            if should_match {
                Self::find_aux(depth + 1, &rest, &node.children[i], total);
            }
        }

        // Then start the searches for non-wildcard matches: recurse on current-level matches
        if let Some(i) = node.children.iter().position(|n| part.eq(&n.data)) {
            Self::find_aux(depth + 1, &rest, &node.children[i], total)
        }
    }
}

// There are now more lines of test code for this structure than for the structure (about 2:1).
//
// I don't know how to split up tests in Rust.
#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn insert_and_get() {
        let mut t = TopicFilterTree::new();
        t.insert("foo/bar", 3).unwrap();
        t.insert("foo/bar", 2).unwrap();
        t.insert("foo/bar/baz", 3).unwrap();
        t.insert("long/path/to/the/end", 7).unwrap();
        t.insert("long/path/from/the/end", 7).unwrap();
        t.insert("bar/foo", 1).unwrap();

        // This is only displayed when an assertion fails--wow!
        tests::show(&mut t);

        assert_eq!(0, t.find("should/not/exist").len());
        assert_eq!(vec!(3, 2), t.find("foo/bar"));
        assert_eq!(vec!(7), t.find("long/path/to/the/end"));
    }

    // https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901247

    /// A Topic Name or Topic Filter consisting only of the ‘/’ character is valid
    #[test]
    fn topic_name_can_be_single_slash() {
        let mut t = TopicFilterTree::new();

        t.insert("/", 0).unwrap();
        assert_eq!(vec!(0), t.find("/"));
    }

    /// Topic Names and Topic Filters MUST NOT include the null character (Unicode U+0000).
    #[test]
    fn topic_names_must_not_include_unicode_null() {
        let mut t = TopicFilterTree::new();

        let mut topic = String::new();
        topic.push(std::char::from_u32(0x0000).unwrap());
        assert!(t.insert(&topic, 0).is_err());
    }

    /// Topic Names and Filters are UTF-8 Strings and MUST NOT encode to more than 65,535 bytes.
    #[test]
    fn topic_names_must_be_less_than_2_32_bytes() {
        let mut t = TopicFilterTree::new();

        t.insert("foo", 0).unwrap();
        assert_eq!(0, t.find("Foo").len());
    }

    /// Topic Names and Topic Filters are case sensitive.
    #[test]
    fn topic_names_are_case_sensitive() {
        let mut t = TopicFilterTree::new();

        t.insert("foo", 0).unwrap();
        assert_eq!(0, t.find("Foo").len());
    }

    /// All Topic Names and Topic Filters MUST be at least one character long.
    #[test]
    fn topic_names_must_be_at_least_one_character_long() {
        let mut t = TopicFilterTree::new();

        assert!(t.insert("", 0).is_err());
    }

    /// Topic Names and Topic Filters can include the space character.
    #[test]
    fn topic_names_can_include_spaces() {
        let mut t = TopicFilterTree::new();

        t.insert("foo bar", 1).unwrap();
        assert_eq!(vec!(1), t.find("foo bar"));
    }

    /// A leading or trailing ‘/’ creates a distinct Topic Name or Topic Filter.
    #[test]
    fn topic_names_slashes_differentiate_topics() {
        let mut t = TopicFilterTree::new();

        t.insert("slash", 0).unwrap();
        t.insert("/slash", 1).unwrap();
        t.insert("slash/", 2).unwrap();
        assert_eq!(vec!(0), t.find("slash"));
        assert_eq!(vec!(1), t.find("/slash"));
        assert_eq!(vec!(2), t.find("slash/"));
    }

    /// https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901244
    /// (‘#’ U+0023) is a wildcard character matching any number of levels within a topic.
    /// The multi-level wildcard represents the parent and any number of child levels.
    #[test]
    fn multi_level_wildcard_happy() {
        let mut t = TopicFilterTree::new();
        t.insert("#", 0).unwrap();
        t.insert("one", 1).unwrap();
        t.insert("one/#", 2).unwrap();
        t.insert("one/one", 3).unwrap();

        assert_eq!(vec!(0, 1, 2), t.find("one"));
        assert_eq!(vec!(0, 2, 3), t.find("one/one"));
        assert_eq!(vec!(0, 2), t.find("one/two"));
    }

    /// https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901244
    /// (The multi-level wildcard) MUST be the last character specified in the Topic Filter.
    #[test]
    fn multi_level_wildcard_only_allowed_at_end() {
        let mut t = TopicFilterTree::new();

        assert!(t.insert("one/#/one", 0).is_err());
    }

    /// https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901244
    /// # MUST be specified either on its own or following a topic level separator.
    #[test]
    fn multi_level_wildcard_only_replaces_levels() {
        let mut t = TopicFilterTree::new();

        assert!(t.insert("one#", 0).is_err());
    }

    /// https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901245
    /// The plus sign (‘+’ U+002B) is a wildcard character that matches only one topic level.
    #[test]
    fn single_level_wildcard_happy() {
        let mut t = TopicFilterTree::new();
        t.insert("+", 0).unwrap();
        t.insert("one", 1).unwrap();
        t.insert("one/+", 2).unwrap();
        t.insert("one/one", 3).unwrap();
        t.insert("one/+/one", 4).unwrap();
        t.insert("one/one/one", 5).unwrap();
        t.insert("one/+/one/+", 6).unwrap();

        assert_eq!(vec!(0, 1), t.find("one"));
        assert_eq!(vec!(0), t.find("two"));
        assert_eq!(vec!(2, 3), t.find("one/one"));
        assert_eq!(vec!(2), t.find("one/two"));
        assert_eq!(vec!(4, 5), t.find("one/one/one"));
        assert_eq!(vec!(4), t.find("one/two/one"));
        assert_eq!(vec!(6), t.find("one/two/one/one"));
        assert_eq!(vec!(6), t.find("one/two/one/two"));
    }

    #[test]
    fn single_level_wildcard_boundaries() {
        let mut t = TopicFilterTree::new();
        t.insert("+", 0).unwrap();
        t.insert("one", 1).unwrap();
        t.insert("one/+", 2).unwrap();

        assert_eq!(vec!(0, 1), t.find("one"));
        assert_eq!(vec!(0), t.find("two"));
        assert_eq!(vec!(2), t.find("one/"));
        assert_eq!(vec!(2), t.find("one/two"));
    }

    #[test]
    fn single_level_wildcard_delimiters() {
        let mut t = TopicFilterTree::new();
        t.insert("+", 1).unwrap();
        t.insert("/+", 2).unwrap();
        t.insert("+/+", 3).unwrap();

        assert_eq!(vec!(3, 2), t.find("/one"));
    }

    #[test]
    fn single_and_multi_level_wildcards() {
        let mut t = TopicFilterTree::new();
        t.insert("+/one/#", 0).unwrap();

        assert_eq!(vec!(0), t.find("one/one/two"));
        assert_eq!(vec!(0), t.find("one/one/two/two"));
        assert_eq!(0, t.find("one/two/two/two").len());
    }

    /// https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901246
    /// MUST NOT match filters starting with a wildcard with topics beginning with a $
    #[test]
    fn start_wildcards_do_not_match_system_topics() {
        let mut t = TopicFilterTree::new();
        t.insert("#", 0).unwrap();
        t.insert("+/one", 1).unwrap();
        t.insert("$one/#", 2).unwrap();
        t.insert("$one/one/+", 3).unwrap();

        assert_eq!(vec!(2, 3), t.find("$one/one/one"));
        assert_eq!(vec!(2), t.find("$one/one"));
    }

    fn show(tree: &mut TopicFilterTree<i32>) {
        tests::print_node(&mut tree.root, String::from("").as_str())
    }

    fn print_node(node: &mut Node<i32>, prefix: &str) {
        for child in node.children.iter_mut() {
            println!("{}{} ({:?})", prefix, child.data, child.subscribers);
            print_node(child, &format!("{} ", prefix));
        }
    }
}
